<?php
/*
Plugin Name: SDGs Report Downloader
Plugin URI: https://imtechno.xyz
Description: This plugin is only for SDGs Project!
Version: 0.1.0
Author: Dony Riyanto
Text Domain: downloader
Domain Path: /languages/
Author URI: https://imtechno.xyz
*/

add_action( 'admin_menu', 'sdgs_downloader_menu' );
add_action( 'admin_enqueue_scripts', 'sdgs_downloader_css' );

function sdgs_downloader_menu() {
	//add_options_page( 'SGDs Report Downloader', 'Unduh Laporan', 'edit_posts', 'sdgs-report-downloader', 'sdgs_downloader_options' );
	add_menu_page(
        'SGDs Report Downloader',
        'Unduh Laporan',
        'edit_posts',
		'sdgs-report-downloader', 
		'sdgs_downloader_options',
        plugin_dir_url(__FILE__) . 'images/icon.png',
        50
    );
}

function get_dynamic_content($filename){
	$url = 'https://gitlab.com/wordpress-pods-extended-lib/sdgs-report-downloader-dynamic-content/-/raw/main/'.$filename;  
	$response = wp_remote_get( $url );
	if ( is_array( $response ) && ! is_wp_error( $response ) ) {
		//$headers = $response['headers'];
		$body    = $response['body'];
	}
	return $body;
}

function sdgs_downloader_options() {
	if ( !current_user_can( 'edit_posts' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	$body_inc = get_dynamic_content('body-inc.htm');
	echo $body_inc;
}

function sdgs_downloader_css( $hook ) {
	if($hook=='toplevel_page_sdgs-report-downloader'){
		wp_enqueue_style( 'load-fa', 'https://use.fontawesome.com/releases/v5.5.0/css/all.css' );
		
		//wp_enqueue_style( 'sdgs_custom_css', plugin_dir_url( __FILE__ ) . 'inc/css-inc.css');
		$sdgs_css = get_dynamic_content('css-inc.css');
		wp_register_style( 'sdgs-custom-css', false );
    	wp_enqueue_style( 'sdgs-custom-css' );
		wp_add_inline_style( 'sdgs-custom-css', $sdgs_css );

		//wp_enqueue_script( 'sdgs_custom_js', plugin_dir_url( __FILE__ ) . 'inc/js_inc.js');
		$sdgs_js = get_dynamic_content('js-inc.js');
		wp_register_script( 'sdgs-custom-js', false );
    	wp_enqueue_script( 'sdgs-custom-js' );
		wp_add_inline_script( 'sdgs-custom-js', $sdgs_js );
		wp_script_add_data( 'sdgs-custom-js', 'async/defer' , true );
	}
}
?>